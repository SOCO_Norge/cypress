# Cypress

## Installere programmer
1. Installer Node.js https://nodejs.org/en/download/
2. Installer git https://git-scm.com/download/win

## Clone Repo (kopire innhold)
1. Lag en folder som heter _git_ (f.eks `c:/git/`)
4. Åpne powershell og skriv `cd c:/git`
5. skriv så `git clone https://Roger-h-martinsen@bitbucket.org/SOCO_Norge/cypress.git`. Dette vil kopiere dette innholdet til en folder som heter "cypress" `c:/git/cypress`

## Tutorial
### Komme igang
1. Gå til "cypress"-folderen og skriv "git checkout starten". Dette vil hente ut "branch"en som heter "starten". Dette er vårt utgangspunkt for opplæringen.
2. Installer "cypress" (og evt. andre pakker).
    1. Gå til `c:/git/cypress`
    2. Skriv  `npm install`. Dette vil installere pakker vi trenger, som definert i `package.json`, listet under `devDependencies` eller `Dependencies`
    3. Skriv `npx cypress install`

### Lag et script for å åpne SOCOs hjemmeside
TBD 

### Lag et script for å sjekke om vi er norges testpartner
TBD

### Lag et script for å sjekke om du er en del av SOCO-teamet
TBD
