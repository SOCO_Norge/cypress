describe('Jeg kan navigere til siden for å vise ansatte', () => {
    before(() => {
        cy.visit("http://soco.no")
        cy.get(".menu-button").click()
        cy.contains("Om oss").click()
        cy.get(".side-nav").contains("Medarbeidere").click({force:true});
    })

    it('Jeg forventer å finne oversikt over anstatte', () => {
        cy.url().should("be.equal", "http://soco.no/ansatte")
    })  
})